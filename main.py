# Pygame tutorial https://coderslegacy.com/python/python-pygame-tutorial/

# Import packages
import pygame  # main pygame package
from pygame.locals import *  # to allow using pygame.locals without the prefix
import sys
import random
import time

# Initiate pygame ----
pygame.init()

# Define sizes
SCREEN_WIDTH = 400
SCREEN_HEIGHT = 600
VPADDING = 40
HPADDING = 30


# Define colours
BLACK = pygame.Color(0, 0, 0)
WHITE = pygame.Color(255, 255, 255)
PINK = pygame.Color(226, 31, 255)
LIGHT_BLUE = pygame.Color(31, 226, 255)
GREEN = pygame.Color(0, 250, 0)
RED = pygame.Color(250, 0, 0)
GREY = pygame.Color(128, 128, 128)


# Set the frames per second
FRAMES_PER_SECOND = 60
fps = pygame.time.Clock()

# Define initial speed
speed = 5
bg = 1

# Setting up Fonts
font = pygame.font.SysFont("Verdana", 60)
font_small = pygame.font.SysFont("Verdana", 20)
game_over = font.render("Game Over", True, BLACK)

background = pygame.image.load("AnimatedStreet.png")
background2 = pygame.image.load("AnimatedStreet2.png")

# Create the game elements

class Enemy(pygame.sprite.Sprite):
    def __init__(self, speed):
        super().__init__()
        self.image = pygame.image.load("Enemy.png")
        self.rect = self.image.get_rect()
        self.rect.center = (random.randint(VPADDING, SCREEN_HEIGHT - VPADDING), 0)
        self.speed = speed

    def move(self, score):
        self.rect.move_ip(0, self.speed)
        if (self.rect.bottom > SCREEN_HEIGHT):
            score += 1
            self.rect.top = 0
            self.rect.center = (random.randint(HPADDING, SCREEN_WIDTH - HPADDING), 0)
        return score

    def draw(self, surface):
        surface.blit(self.image, self.rect)

class Player(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.image.load("Player.png")
        self.rect = self.image.get_rect()
        self.rect.center = (160, 520)
        self.score = 0

    def move(self):
        keys = pygame.key.get_pressed()
        if keys[K_UP]:
            self.rect.move_ip(0, -5)
        if keys[K_DOWN]:
            self.rect.move_ip(0, 5)
        if self.rect.left > 0:
            if keys[K_LEFT]:
                self.rect.move_ip(-5, 0)
        if self.rect.right < SCREEN_WIDTH:
            if keys[K_RIGHT]:
                self.rect.move_ip(5, 0)

    def draw(self, surface):
        surface.blit(self.image, self.rect)


# Create the base surface
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption("basic tutorial")

# Create sprites
p1 = Player()
e1 = Enemy(speed)

# Create sprite groups
enemies = pygame.sprite.Group()
enemies.add(e1)
all_sprites = pygame.sprite.Group()
all_sprites.add(p1)
all_sprites.add(e1)


# Add a new user event
increase_speed = pygame.USEREVENT + 1  # create a new type of event
pygame.time.set_timer(increase_speed, 10000)


# Game loop ----
while True:
    # quitting the game
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()  # closes the pygame window
            sys.exit()  # stops the script
        if event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                pygame.quit()
                sys.exit()
        if event.type == increase_speed:
            for enemy in enemies:
                enemy.speed += 0.5

    screen.fill(WHITE)

    if (bg % 60 < 30):
        screen.blit(background, (0, 0))
        bg += 1
    else:
        screen.blit(background2, (0, 0))
        bg += 1

    scores = font_small.render(str(p1.score), True, BLACK)
    screen.blit(scores, (10, 10))

    # update elements
    for entity in all_sprites:

        entity.draw(screen)

        if entity in enemies:
            p1.score = entity.move(p1.score)
        else:
          entity.move()

    if random.randint(1, 4000) == 1:
        # Create sprite groups
        e = Enemy(speed)
        enemies.add(e)
        all_sprites.add(e)

    # detect collisions
    if pygame.sprite.spritecollideany(p1, enemies):
        pygame.mixer.Sound('crash.wav').play()
        screen.fill(RED)
        screen.blit(game_over, (30, 250))
        pygame.display.update()
        time.sleep(1)

# game_over = font.render("Game Over", True, BLACK)

        screen.fill(WHITE)
        scores = font.render("Score: " + str(p1.score), True, BLACK)
        screen.blit(scores, (50, 250))
        pygame.display.update()
        time.sleep(3)

        for entity in all_sprites:
            entity.kill()

        pygame.quit()
        sys.exit()

    # Display the changes
    pygame.display.update()
    fps.tick(FRAMES_PER_SECOND)


